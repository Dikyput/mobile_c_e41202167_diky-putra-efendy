void main() {
  for (var i = 1 ; i <=20 ; i++) {
    if (i % 3 ==0 && i % 2 == 1) {
      print('$i - I Loved Dart Programming');
    } else {
      print((i / 2 == 1) ? '$i - Amatir' : '$i - Professional');
    }
  }
}
