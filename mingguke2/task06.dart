import 'dart:io';

void main() {
  stdout.write("Input Nama: ");
  var name = stdin.readLineSync();

  stdout.write("Input Hero: ");
  var hero = stdin.readLineSync();

  if (name == '') {
    print('Mohon Inputkan namamu!');
  } else if (hero == '') {
    print('Halo $name, Pilih Heromu untuk memulai permainan Warewolf!');
  } else if (hero == 'Marksman') {
    print(
        'Selamat datang di Dunia Werewolf, $name\nHalo $hero $name, kamu dapat melihat siapa yang menjadi werewolf!');
  } else if (hero == 'Mage') {
    print(
        'Selamat datang di Dunia Werewolf, $name\nHalo $hero $name, kamu akan membantu melindungi temanmu dari serangan werewolf!');
  } else if (hero == 'Tanker') {
    print(
        'Selamat datang di Dunia Werewolf, $name\nHalo $hero $name, Kamu akan berada dibarisan terdepan setiap !');
  } else {
    print('Gagal!');
  }
}