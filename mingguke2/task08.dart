void main() {
  var hari = 28;
  var bulan = 2;
  var tahun = 20;

  if (hari > 0 ||
      hari<32 && bulan>0 ||
      bulan<13 && tahun>1900 ||
      tahun<2200) {
    switch (bulan) {
        case 1:
          print("$hari JANUARI $tahun");
          break;
        case 2:
          print("$hari FEBRUARI $tahun");
          break;
        case 3:
          print("$hari MARET $tahun");
          break;
        case 4:
          print("$hari APRIL $tahun");
          break;
        case 5:
          print("$hari MEI $tahun");
          break;
        case 6:
          print("$hari JUNI $tahun");
          break;
        case 7:
          print("$hari JULI $tahun");
          break;
        case 8:
          print("$hari AGUSTUS $tahun");
          break;
        case 9:
          print("$hari SEPTEMBER $tahun");
          break;
        case 10:
          print("$hari OKTOBER $tahun");
          break;
        case 11:
          print("$hari NOVEMBER $tahun");
          break;
        case 12:
          print("$hari DESEMBER $tahun");
          break;
    }
  } else {
    print("Data tidak sesuai.");
  }
}
