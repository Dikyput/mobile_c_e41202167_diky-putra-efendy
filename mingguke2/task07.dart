import 'dart:io';

void main() {
  print("Masukkan Kotamu untuk mengetahui Cafe yang terkenal dikotamu ex: (Surabaya)!");
  String city = stdin.readLineSync()!;
  String cafe;

  switch (city) {
    case "Surabaya":
      {
        cafe =
            "Noach Cafe and Bistro";
        break;
      }
    case "Malang":
      {
        cafe =
            "Golden Heritage Koffie";
        break;
      }
    case "Jember":
      {
        cafe =
            "Brother's Coffee Bar";
        break;
      }
    case "Jakarta":
      {
        cafe =
            "Sky Garden Cafe";
        break;
      }
    case "Banyuwangi":
      {
        cafe = "Cafe Only Me";
        break;
      }
    case "Solo":
      {
        cafe = "Tiga Tjeret Cafe";
        break;
      }
    case "Bali":
      {
        cafe =
            "Scrummy Cafe & Bakery";
        break;
      }
    default:
      {
        cafe = "Kota $city Belum Tersurvei :)";
      }
  }
  print(cafe);
}