import 'dart:ui';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Belajar E41202167'),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            icon(),
            tombol(),
            textformfield(),
            tulisan(),
            gambar(),
          ],
        ),
      ),
    );
  }

  Widget tulisan() {
    return const Center(
      child: Text(
        "Register?",
        style: TextStyle(
          color: Color.fromARGB(255, 255, 217, 0),
          backgroundColor: Color.fromARGB(255, 255, 255, 255),
          fontSize: 20.0,
          fontStyle: FontStyle.italic,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget icon() {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 30,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            children: const [
              Icon(Icons.message_rounded),
              Text('Message'),
            ],
          ),
          Column(
            children: const [
              Icon(Icons.phone),
              Text('Phone'),
            ],
          ),
          Column(
            children: const [
              Icon(Icons.book),
              Text('Book'),
            ],
          ),
        ],
      ),
    );
  }

  Widget tombol() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RaisedButton(
            color: Colors.blue,
            child: const Text("Button 1"),
            onPressed: () {},
          ),
          const SizedBox(height: 25),
          MaterialButton(
            color: Colors.yellow,
            child: const Text("Button 2"),
            onPressed: () {},
          ),
          const SizedBox(height: 25),
          FlatButton(
            color: Colors.green,
            child: const Text("Button 3"),
            onPressed: () {},
          ),
        ],
      ),
    );
  }

  Widget textformfield() {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 40,
        vertical: 10,
      ),
      child: Form(
        child: Column(
          children: [
            TextFormField(
              decoration: const InputDecoration(hintText: "Username"),
            ),
            TextFormField(
              obscureText: true,
              decoration: const InputDecoration(hintText: "Password"),
            ),
            const SizedBox(height: 30),
            RaisedButton(
              child: const Text("Login"),
              onPressed: () {},
            )
          ],
        ),
      ),
    );
  }

  Widget gambar() {
    return Center(
      child: Image.asset('assets/gambar/r.jpg'),
    );
  }
}
