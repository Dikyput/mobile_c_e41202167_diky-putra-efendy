void main() {
  hello();
  print('----------------------------------------');
  hello2('DIKY PUTRA EFENDY', 7);
  print('----------------------------------------');
  hello2('DIKY PUTRA EFENDY', 7);
}

void hello() {
  print('Hallo, Diky Putra!');
}

void hello2(String name, int day) {
  print('Hallo, $name!');
  print('Anda sedang belajar di pertemuan ke $day.');
}

void hello3(String name, int day) =>
    'Hallo, $name! \n Anda sedang belajar di pertemuan ke $day.';