import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(home: const MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => Gradient();
}

class Gradient extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Flexible(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: <Color>[
                      Colors.orange,
                      Colors.red,
                    ],
                  ),
                ),
              )),
          Flexible(
              flex: 2,
              child: Container(
                decoration: BoxDecoration(
                  gradient: RadialGradient(
                    radius: 2.0,
                    colors: <Color>[
                      Colors.orange,
                      Colors.red,
                    ],
                  ),
                ),
              )),
          Flexible(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                  gradient: SweepGradient(
                    startAngle: 0.2,
                    colors: <Color>[
                      Colors.orange,
                      Colors.red,
                      Colors.redAccent,
                    ],
                  ),
                ),
              ))
        ],
      ),
    );
  }
}

class PageView extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Flexible(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: <Color>[
                      Colors.orange,
                      Colors.red,
                    ],
                  ),
                ),
              )),
          Flexible(
              flex: 2,
              child: Container(
                decoration: BoxDecoration(
                  gradient: RadialGradient(
                    radius: 2.0,
                    colors: <Color>[
                      Colors.orange,
                      Colors.red,
                    ],
                  ),
                ),
              )),
          Flexible(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                  gradient: SweepGradient(
                    startAngle: 0.2,
                    colors: <Color>[
                      Colors.orange,
                      Colors.red,
                      Colors.redAccent,
                    ],
                  ),
                ),
              ))
        ],
      ),
    );
  }
}
