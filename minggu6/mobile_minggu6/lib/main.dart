import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(MaterialApp(home: const MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final List<String> g = ["g1.jpg", "g2.jpg", "g3.jpg", "g4.png", "g5.png"];

  static const Map<String, Color> colors = {
    'g1.jpg': Color(0xFF2DB569),
    'g2.jpg': Color(0xFFF38688),
    'g3.jpg': Color(0xFF45CAF5),
    'g4.png': Color(0xFFB19ECB),
    'g5.png': Color(0xFFF58E4C),
  };

  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: FractionalOffset.topCenter,
              end: FractionalOffset.bottomCenter,
              colors: [Colors.white, Colors.blue, Colors.blueGrey]),
        ),
        child: PageView.builder(
            controller: PageController(viewportFraction: 0.8),
            itemCount: g.length,
            itemBuilder: (BuildContext context, int i) {
              return Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 5.0, vertical: 50.0),
                  child: Material(
                    elevation: 8.0,
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Hero(
                            tag: g[i],
                            child: Material(
                              child: InkWell(
                                child: Flexible(
                                  flex: 1,
                                  child: Container(
                                    color: colors.values.elementAt(i),
                                    child: Image.asset(
                                      "img/${g[i]}",
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                onTap: () => Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            Halamandua(
                                              g: g[i],
                                              colors:
                                                  colors.values.elementAt(i),
                                            ))),
                              ),
                            ))
                      ],
                    ),
                  ));
            }),
      ),
    );
  }
}

class Halamandua extends StatelessWidget {
  Halamandua({Key? key, required this.g, required this.colors})
      : super(key: key);
  final String g;
  final Color colors;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PAGE GAME"),
        backgroundColor: Colors.blue,
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                gradient: RadialGradient(
                    center: Alignment.center,
                    colors: [Colors.blue, Colors.white, Colors.blueGrey])),
          ),
          Center(
            child: Hero(
                tag: g,
                child: ClipOval(
                    child: SizedBox(
                        width: 200.0,
                        height: 200.0,
                        child: Material(
                          child: InkWell(
                            onTap: () => Navigator.of(context).pop(),
                            child: Flexible(
                              flex: 1,
                              child: Container(
                                color: colors,
                                child: Image.asset(
                                  "img/$g",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        )))),
          )
        ],
      ),
    );
  }
}
