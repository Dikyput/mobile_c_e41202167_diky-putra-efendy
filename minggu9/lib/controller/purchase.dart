import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import '/model/product.dart';

class Purchase extends GetxController {
  var products = <Product>[].obs;
  @override
  void onInit() {
    fetchProducts();
    super.onInit();
  }

  void fetchProducts() async {
    await Future.delayed(Duration(seconds: 1));
    var serverResponse = [
      Product(1, "300 VP", "dikyput",
          "Stok Ready", 33000.0),
      Product(1, "625 VP", "dikyput",
          "Stok Ready", 65000.0),
      Product(1, "1125 VP", "dikyput",
          "Stok Ready", 115000.0),
      Product(1, "1650 VP", "dikyput",
          "Stok Ready", 160000.0),
      Product(1, "2275 VP", "dikyput",
          "Stok Ready", 225000.0),
      Product(1, "3400 VP", "dikyput",
          "Stok Ready", 315000.0)
    ];
    products.value = serverResponse;
  }
}
