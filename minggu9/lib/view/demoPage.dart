import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/controller/demoController.dart';

class DemoPage extends StatelessWidget {
  final DemoController ctrl = Get
      .find(); 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Halaman Demo'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(Get.arguments
                  .toString()), 
            ),
            SwitchListTile(
              value: ctrl.isDark,
              title: Text("Sentuh untuk mengubah Mode Tema"),
              onChanged: ctrl.changeTheme,
            ),
            ElevatedButton(
                onPressed: () => Get.snackbar(
                    "Snackbar", "Membeli Snackbar",
                    snackPosition: SnackPosition.BOTTOM,
                    colorText: Colors.white,
                    backgroundColor: Colors.black87),
                child: Text('Snack Bar')),
            ElevatedButton(
                onPressed: () => Get.defaultDialog(
                    title: "Bot",
                    content: Text(
                      'Hai, Apakah ada yang bisa dibantu :)',
                    )),
                child: Text('Bantuan')),
            ElevatedButton(
                onPressed: () => Get.bottomSheet(Container(
                      height: 150,
                      color: Colors.white,
                      child: Text(
                        'Ini Halaman Bawah',
                        style: TextStyle(fontSize: 30.0),
                      ),
                    )),
                child: Text('Halaman Bawah')),
            ElevatedButton(
                //off named because we want to remove the page stack
                onPressed: () =>
                    Get.offNamed('/'), //for app remove all stack use Get.offALL
                child: Text('Kembali ke halam utama')),
          ],
        ),
      ),
    );
  }
}
